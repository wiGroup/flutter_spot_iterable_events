enum EConsumerType {all, mixpanel, iterable}

extension EConsumerTypeExtension on EConsumerType {
  static const values = {
    EConsumerType.all: 'all',
    EConsumerType.mixpanel: 'mixpanel',
    EConsumerType.iterable: 'iterable',
  };

  String? get value => values[this];
}