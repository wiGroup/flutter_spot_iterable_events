import 'dart:io';

import 'package:flutter_abstract_network_payload/network/http_request_algorithm.dart';
import 'package:flutter_spot_iterable_events/models/event_enums.dart';
import 'package:flutter_spot_iterable_events/network/network.dart';

import 'consumer_enums.dart';

class EventPayLoad extends SpotIterableEventsNetworkPayload {
  String eventName;
  List<EConsumerType> consumerTypeList;
  String authId;
  String token;
  String authToken;
  Map<String, dynamic> eventProperties;
  EventType eventType;

  EventPayLoad({
    required this.eventName,
    this.consumerTypeList = const [],
    required this.authId,
    required this.token,
    required this.authToken,
    this.eventProperties = const {},
    this.eventType = EventType.event,
  });

  @override
  Map<String, dynamic> request() {
    Map<String, dynamic> properties = {};

    if (eventType == EventType.event) {
      properties.addAll({
        'distinct_id': authId,
        'token': token,
      });
      for (var element in eventProperties.entries) {
        properties.addAll({element.key: element.value});
      }
    } else if (eventType == EventType.setProperties) {
      properties.addAll({
        '\$distinct_id': authId,
        '\$token': token,
        '\$set': eventProperties
      });
    } else if (eventType == EventType.incrementProperties) {
      properties.addAll({
        '\$distinct_id': authId,
        '\$token': token,
        '\$add': eventProperties
      });
    }

    List<String> consumerList = [];
    for (var element in consumerTypeList) {
      consumerList.add(element.value ?? '');
    }

    Map<String, dynamic> body = {};
    body.addAll({
      'event': eventName,
      'auth_id': authId,
      'consumers': [consumerList],
      'properties': properties,
    });

    return body;
  }

  @override
  EHttpRequestAlgorithm get requestAlgorithm => EHttpRequestAlgorithm.replace;

  @override
  response(Map<String, dynamic> json) {
    return null;
  }

  @override
  responseWithStatus(Map<String, dynamic> json, int statusCode) {
    return statusCode;
  }

  @override
  Map<String, String> get additionalHeaders =>
      {HttpHeaders.authorizationHeader: 'Bearer ${this.authToken}'};


  @override
  Map<String, String> get pathParameters => {};

  @override
  Map<String, String> get queryParameters => {};
}
