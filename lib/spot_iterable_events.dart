import 'dart:async';

import 'models/consumer_enums.dart';
import 'models/event_enums.dart';
import 'models/event_payload.dart';
import 'network/network.dart';

class SpotIterableEvents {
  static Future<dynamic> trackEvent({
    required String baseUrl,
    String eventName = '',
    required String authId,
    required String token,
    required String authToken,
    required List<EConsumerType> consumerTypeList,
    Map<String, dynamic> eventProperties = const {},
    EventType eventType = EventType.event,
  }) async {
    EventPayLoad payLoad = EventPayLoad(
      eventName: eventName,
      authId: authId,
      token: token,
      authToken: authToken,
      consumerTypeList: consumerTypeList,
      eventProperties: eventProperties,
      eventType: eventType,
    );

    return SpotIterableEventsNetworkLayer()
        .request(baseUrl, SpotIterableEventsRouter.trackEvent, payLoad)
        .then((response) {
      return response;
    });
  }
}
