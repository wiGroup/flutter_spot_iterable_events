import 'package:flutter_abstract_network_payload/network/network.dart';

abstract class SpotIterableEventsNetworkPayload<T> extends NetworkPayload<T> {
  T responseWithStatus(Map<String, dynamic> json, int statusCode);
}
