import 'package:flutter_abstract_network_payload/network/network.dart';

import 'router.dart';

extension SpotIterableEventsRouterMethod on SpotIterableEventsRouter {
  HttpMethod get method {
    switch (this) {
      case SpotIterableEventsRouter.trackEvent:
        return HttpMethod.post;
    }
  }
}
