import 'router.dart';

extension SpotIterableEventsRouterPath on SpotIterableEventsRouter {
  String get path {
    switch (this) {
      case SpotIterableEventsRouter.trackEvent:
        return '/events';
    }
  }
}
