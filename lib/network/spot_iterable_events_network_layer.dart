import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_abstract_network_payload/network/network.dart';

import '../constants.dart';
import 'network.dart';
import 'router/router.dart';

@protected
class SpotIterableEventsNetworkLayer {
  Dio httpClient = Dio(
    BaseOptions(
      /// 1 minutes timeout
      receiveTimeout: 60 * 1000,
    ),
  );

  Future<dynamic> request<T>(String baseUrl, SpotIterableEventsRouter router,
      SpotIterableEventsNetworkPayload payload) async {

    var options = Options(
      headers: {
        HttpHeaders.contentTypeHeader: ContentType.json.value,
      },
      method: router.method.value,
    );
    
    if ( payload.additionalHeaders != null ) {
        options.headers?.addAll(payload.additionalHeaders!);
    }

    Uri url = Uri.https(baseUrl, router.path);

    try {
      final response = await httpClient.request(
        url.toString(),
        data: payload.request(),
        options: options,
      );

      return Future.value(response);
    } catch (e) {
      return Future.error(GENERAL_ERROR_MESSAGE);
    }
  }
}
