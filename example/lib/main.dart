import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spot_iterable_events/models/consumer_enums.dart';
import 'package:flutter_spot_iterable_events/spot_iterable_events.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  int statusCode = 0;
  String response = '';

  String baseUrl = 'eventstest.qaspotplatformapi.com';
  String eventName = 'Viewed Transaction';
  String authId = 'b7c84c71-9fe4-4b82-a712-34adacaeb934';
  String token = 'd691342f093c35800727edee3ec0e2dc';
  String authToken = 'JhbGciOiJSUzI1NiIsI.ey0ZSI6IkEifQ.gRl1OqUv-9a-isnQ';
  List<EConsumerType> consumerTypeList = [EConsumerType.mixpanel];
  Map<String, dynamic> eventProperties = {
    'Transaction Type': "Type TopUP",
    'Public': true,
  };

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TextButton(
              onPressed: () {
                SpotIterableEvents.trackEvent(
                        baseUrl: baseUrl,
                        authId: authId,
                        token: token,
                        authToken: authToken,
                        consumerTypeList: consumerTypeList,
                        eventProperties: eventProperties,
                        eventName: eventName)
                    .then((value) {
                  Response response = value;
                  setState(() {
                    statusCode = response.statusCode ?? 0;
                    this.response = response.data;
                  });
                });
              },
              child: Text('Track Event'),
            ),
            Text(
              'Status code: ' + statusCode.toString(),
            ),
            Padding(
                padding: EdgeInsets.only(top: 10),
                child: Text('Response: ' + response)),
          ],
        ),
      ),
    );
  }
}
