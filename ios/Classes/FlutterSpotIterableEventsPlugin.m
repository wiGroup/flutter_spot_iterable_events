#import "FlutterSpotIterableEventsPlugin.h"
#if __has_include(<flutter_spot_iterable_events/flutter_spot_iterable_events-Swift.h>)
#import <flutter_spot_iterable_events/flutter_spot_iterable_events-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "flutter_spot_iterable_events-Swift.h"
#endif

@implementation FlutterSpotIterableEventsPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftFlutterSpotIterableEventsPlugin registerWithRegistrar:registrar];
}
@end
